from kivy.app import App
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout

class qmonosHome(BoxLayout):
    def init_qmonos(self):
        self.objtype.text = ''
        self.objtype.values = ('Home', 'Work', 'Other', 'Custom')


class qmonosApp(App):
    def build(self):
        Window.clearcolor = (.95,.95,.95,1)
        Window.size = (800, 200)
        homeWin = qmonosHome()
        homeWin.init_qmonos()
        return homeWin

qmonosApp().run()
