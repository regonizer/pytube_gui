from tkinter import filedialog
from tkinter import ttk
from tkinter import *
from downloader import *
from video import *
from contextlib import redirect_stdout
import time

def buildGui():
    # --- root window ---------------------------------------------------------------------
    dl = Downloader()
    window = Tk()
    window.title("Youtube Grabber")
    window.minsize(400,300)

    root = ttk.Frame(window)
    root.pack(fill=BOTH, expand=True)
    root.columnconfigure(1, weight=1)
    root.rowconfigure(2, weight=1)
    root.pack(pady=10, padx=10)
    # -------------------------------------------------------------------------------------


    # --- download directory selector -----------------------------------------------------
    #lbl = Label(root, text="Download Directory:")
    #lbl.grid(column=0, row=0, sticky=E)
    txtDirText = StringVar()
    txtDirText.set(dl.dir)
    txtDir = ttk.Entry(root, state='readonly', textvariable=txtDirText)
    txtDir.grid(column=0, row=0, sticky=E+W, columnspan=2, padx=1, pady=1)
    def selectDir(): 
        filedialog.directory = filedialog.askdirectory() 
        if not filedialog.directory: return
        dl.dir = filedialog.directory
        txtDirText.set(filedialog.directory)
    btnSelectDir = ttk.Button(root, text="change", command=selectDir)
    btnSelectDir.grid(column=2, row=0, sticky=E+W)
    # -------------------------------------------------------------------------------------


    # --- download options ----------------------------------------------------------------
    def addUrl(): 
        dl.addUrl(root.clipboard_get())
        updateListBox()
    btnAddUrl = ttk.Button(root, text="add URL from clipboard", command=addUrl)
    btnAddUrl.grid(column=0, row=1, sticky=W)

    def removeUrl():
        selection = listBox.curselection()
        if len(selection) <= 0: return
        for index in sorted(list(selection), reverse=True):
            dl.removeId(index)
        updateListBox()
        listBox.select_set(selection[0])
    btnRemoveUrl = ttk.Button(root, text="remove", command=removeUrl)
    btnRemoveUrl.grid(column=1, row=1, sticky=W)

    tkvar = StringVar(root)
    choices = { 'MP4a', 'MP4', 'WEBM'}
    popupMenu = ttk.OptionMenu(root, tkvar, dl.target, *choices)
    popupMenu.grid(row=1, column=1, sticky=E)
    def tkvarChange(*args):
        dl.target = tkvar.get()
        tkvar.trace('w', tkvarChange)

    def download():
        dl.downloadAll(updateListBoxDl)
    btnDld = ttk.Button(root, text="+1 downloader", command=download)
    btnDld.grid(column=2, row=1, sticky=E+W)
    # -------------------------------------------------------------------------------------


    # --- listBox -------------------------------------------------------------------------
    listBox = Listbox(root, selectmode=EXTENDED)
    listBox.grid(column=0, row=2, columnspan=3, sticky=N+E+S+W)
    scrollbar = ttk.Scrollbar(root, orient="vertical")
    scrollbar.grid(column=0, row=2, columnspan=3, sticky=N+E+S)
    scrollbar.config(command=listBox.yview)
    listBox.config(yscrollcommand=scrollbar.set)

    def updateListBox():
        #async problem?
        try:
            listBox.delete(0, END)
            for video in dl.videos: 
                    listBox.insert(END, video.title)
        except Exception as err:
            print(traceback.format_exc())
            return
        
    listBoxDl = Listbox(root, selectmode=EXTENDED, height=3)
    listBoxDl.grid(column=0, row=3, columnspan=3, sticky=E+S+W)
    scrollbarDl = ttk.Scrollbar(root, orient="vertical")
    scrollbarDl.grid(column=0, row=3, columnspan=3, sticky=N+E+S)
    scrollbarDl.config(command=listBoxDl.yview)
    listBoxDl.config(yscrollcommand=scrollbarDl.set)
    lastUpdate = time.time()

    def updateListBoxDl(force=False):
        #async problem? 
        try:
            global lastUpdate
            if force: updateListBox()
            if not force and (time.time() - lastUpdate) < 1: return
            lastUpdate = time.time()
            listBoxDl.config(height=len(dl.videosLoading) if len(dl.videosLoading)>3 else 3)
            listBoxDl.delete(0, END)
            for video in dl.videosLoading: 
                listBoxDl.insert(END, f'{video.progress} {video.title}')
        except Exception as err:
            print(traceback.format_exc())
            return
    # -------------------------------------------------------------------------------------
    return root

if __name__ == "__main__":
    with open('error.log', 'w') as f:
        with redirect_stdout(f):
            root = buildGui()
            root.mainloop()