import sys
sys.path.append('./')

from pytube import YouTube, Playlist
from urllib.request import urlopen
from urllib import parse
from video import *
import traceback
import time
import _thread
import re
import os 

class Downloader:

    dir = ""
    videos = []
    videosLoading = []
    target = "WEBM"

    def __init__(self):
        self.dir = os.getcwd()

    def addUrl(self, text):
        for line in text.splitlines():
            if line == "": continue
            self.parse(line)

    def removeId(self, id):
        del self.videos[id]

    def downloadAll(self, callback):
       _thread.start_new_thread(self.__downloadThread, (callback, ))

    def __downloadThread(self, callback):
        while self.videos:
            try:
                video = self.videos.pop(0)
                callback(True)
                self.videosLoading.append(video)
                def __show_progress_bar(stream, chunk, file_handle, bytes_remaining):
                    current = ((stream.filesize - bytes_remaining)/stream.filesize)
                    video.progress = ('{0:.1f}%').format(current*100)
                    callback()
                for i in range(5):
                    try:
                        yt = YouTube(video.url)
                        safeTitle = re.sub(r'[^\x00-\x7F]+',' ', yt.title)
                        video.title = f" {safeTitle}"
                        yt.register_on_progress_callback(__show_progress_bar)
                        yt.currentVideo = video
                        if self.target == 'MP4':
                            file = yt.streams \
                                .filter(progressive=True, subtype='mp4') \
                                .order_by('resolution') \
                                .desc() \
                                .first() \
                                .download(self.dir)
                        elif self.target == 'MP4a':
                            file = yt.streams \
                                .filter(only_audio=True, subtype='mp4') \
                                .order_by('abr') \
                                .desc() \
                                .first() \
                                .download(self.dir)
                        elif self.target == 'WEBM':
                            file = yt.streams \
                                .filter(only_audio=True, subtype='webm') \
                                .order_by('abr') \
                                .desc() \
                                .first() \
                                .download(self.dir)
                        self.videosLoading.remove(video)
                        break
                    except Exception as err:
                        print(traceback.format_exc())
                        time.sleep(3) # youtube has a limits the requests per time
                        video.progress = 'FAIL'
                        callback()
            except Exception as err:
                print(traceback.format_exc())
            callback(True)

    def parse(self, url):
        parsed = parse.parse_qs(parse.urlparse(url).query)
        if 'list' in parsed:
            listid = parsed['list'][0]
            page = urlopen(url).read()
            hrefs = re.findall('(?<=href\=").*?(?=")', str(page))
            urls = [v for v in hrefs if listid in v and 'watch?v=' in v]
            urls = [re.findall('(?<=watch\?v\=).*?(?=&amp;list)', v)[0] for v in urls]
            urls = list(set(urls))
            [self.parse('https://www.youtube.com/watch?v=' + v) for v in urls]
        elif 'watch?v=' in url:
            title = re.findall('(?<=watch\?v\=).*', url)[0]
            self.videos.append(Video(url, title))